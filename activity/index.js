
//create an object using obj lit
let trainer = {}

//properties

trainer.name = 'Ash Ketchum';
trainer.age = 10;
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle','Bulbasaur']
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
}

//Methods

trainer.talk = function(){
	console.log('Pikachu, I choose you')
}
trainer.talk();

//Check if all props are added

console.log(trainer);

//Access object properties using dot notation
console.log('Result of dot notation: ');
console.log(trainer.name);
//Access object properties using square brackets

console.log('Result of square bracket notation: ');
console.log(trainer['pokemon'])

//Access the trainer"talk" method
console.log('Result of talk method: ');
trainer.talk();


//Create a constructor function for creating a pokemon

function pokemon(name, level){

	//Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target){
		console.log(this.name + " taclked " + target.name);
		target.health -= this.attack;

		console.log(target.name + "'s health is not reduced to " + target.health)

		if(target.health <= 0){
			target.faint()
		}
	};
	this.faint = function(){
		console.log(this.name + " fainted");
	}
}

//Instantiate a new pokemon

let pikachu = new pokemon ("Pikachu", 12)
console.log(pikachu);

//
let raichu = new pokemon("raichu", 15)
console.log(raichu);

let missingno = new pokemon ("Missingno", 18);
console.log(missingno);

// invoke the tackle method

raichu.tackle(pikachu);
console.log(pikachu);

missingno.tackle(pikachu);
console.log(pikachu)